# Документация

- [Как настроить рабочее окружение](setup.md)
- [Как сдавать задачи](ci.md)
- [Работа с CLion](clion.md)
- [Работа с докер-контейнером](docker.md)
