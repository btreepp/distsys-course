#include <rsm/raft/peer_base.hpp>

namespace raft {

using whirl::rpc::TChannel;

size_t PeerBase::ClusterSize() const {
  return channels_.size();
}

ServerId PeerBase::MyId() const {
  return Id();
}

void PeerBase::ConnectToPeers() {
  auto addresses = DiscoverCluster();
  for (size_t i = 0; i < addresses.size(); ++i) {
    ServerId id = i + 1;  // We need Configuration!

    if (id != MyId()) {
      peer_ids_.push_back(id);
    }

    auto transport_channel = RPCClient()->Dial(addresses[i]);
    channels_.emplace(id, transport_channel);
  }
}

const std::vector<ServerId>& PeerBase::PeerIds() const {
  return peer_ids_;
}

TChannel PeerBase::PeerChannel(ServerId id) {
  return channels_.at(id);
}

const std::string& PeerBase::PeerName(ServerId id) const {
  return channels_.at(id)->Peer();
}

}  // namespace raft