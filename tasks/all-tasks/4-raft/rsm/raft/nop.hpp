#pragma once

#include <whirl/cereal/serialize.hpp>
#include <whirl/cereal/empty_message.hpp>

#include <rsm/raft/command.hpp>

namespace raft {

struct Nop {
  using Request = whirl::EmptyMessage;
  using Response = whirl::EmptyMessage;
};

// Append Nop command to log in BecomeLeader
// to guarantee progress under RAFT commit rule

inline Command MakeNopCommand() {
  return {RequestId{"RAFT", 1}, "Nop", Bytes::Serialize(Nop::Request{})};
}

}  // namespace raft
