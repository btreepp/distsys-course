#pragma once

#include <string>

namespace kv {

// string -> int32
using Key = std::string;
using Value = int32_t;

}  // namespace kv
