cmake_minimum_required(VERSION 3.5)

begin_task()
add_task_library(rsm whirl)
add_task_test_dir(tests-1)
add_task_test_dir(tests-2)
add_task_test_dir(tests-3)
end_task()
